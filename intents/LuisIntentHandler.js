// bot dependencies
var builder = require('botbuilder');
var Util = require("../utils/Functions");

// app config 
var config = require('../config');

/* LUIS API Intent Handler Controller */
var LuisIntentHandler = function (intent) {
    this.LOG_TAG = "LuisIntentHandler::";
    this.init();
};

LuisIntentHandler.prototype =  {
    init: function () {
        var self = this;
        var intent = new builder.IntentDialog();
        console.log(this.LOG_TAG + "::init");
        
        // Create LUIS recognizer that points at our model and add it as the root '/' dialog for our Cortana Bot.
        var model = config.LUIS_URL;
        var recognizer = new builder.LuisRecognizer(model);
        var intent = new builder.IntentDialog({ recognizers: [recognizer], intentThreshold: 0.6 });

        // set up intent handlers
        intent.matches(/(hola+)|(Hola+)/, [
            function (session) {
                session.beginDialog("firstRun");
            }
        ]);

        // Add intent handlers
        intent.matches('BusquedaLaboral', [
            function (session, args, next) {
                console.log(args);
                console.log('Luis: Intent Matches BusquedaLaboral');
                var luisData = self.resolveLuisData(args);
                
                if (luisData.Nombre && luisData.Nombre.length > 0) {
                    session.userData.name = luisData.Nombre;
                }

                if (luisData.Perfil && luisData.Perfil.length > 0) {
                    session.conversationData.profile = luisData.Perfil;
                }

                console.log(self.LOG_TAG + " BusquedaLaboral:: data");
                console.log(session.conversationData);

                session.beginDialog("/hr_profile");
            }
        ]);
        
        intent.matches('NuevoNegocio', [ 
            function (session, args, next) {
                console.log('Luis: Intent Matches NuevoNegocio');
            }
        ]);
        
        intent.matches('PropuestaDeServicios', [
            function (session, args, next) {
                console.log('Luis: Intent Matches PropuestaDeServicios');
            }
        ]);
        
        intent.matches('SobreWunderman', [
            function (session, args, next) {
                console.log('Luis: Intent Matches SobreWunderman');
                var luisData = self.resolveLuisData(args);

                if (luisData.People && luisData.People.length > 0) {
                    session.conversationData.People = luisData.People;
                    session.beginDialog("about-luis")
                } else {
                    // clear previous stored values
                    session.conversationData.People = null;
                    session.beginDialog("about");
                }
            }
        ]);
        
        // if non of Luis Intent Matches try the Random Intent Handler as Default.
        intent.onDefault([
            function (session, args, next) {
                console.log(self.LOG_TAG + "::onDefault: begin Random");
                // redirects the intent to RandomIntentHandler
                session.replaceDialog("/random");
            },

            function (session, args, next) {
                console.log(self.LOG_TAG + "::onDefault:last step");
                session.endDialog("qué lindo día para hacer un asado!");
            }
        ]);

        GlobalBotInstance.dialog('/', intent);
    },

    /* Process  LUIS response */
    resolveLuisData: function (luisData) {
        var data = {};

        if (luisData.entities && luisData.entities.length > 0) {
            for (var i = 0; i < luisData.entities.length; i ++) {
                data[luisData.entities[i].type] = luisData.entities[i].entity;
            }
        }

        return data;
    }
};

module.exports = new LuisIntentHandler ();