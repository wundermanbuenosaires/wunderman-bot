// bot dependencies
var builder = require('botbuilder');
var randomReplies = require("./RandomReplies.json");

/* Bot Random Intent Handler Controller */
var RandomIntentHandler = function (intent) {
    this.LOG_TAG = "RandomIntentHandler::";
    this.init();
};

RandomIntentHandler.prototype =  {
    init: function () {
        var self = this;
        var intent = new builder.IntentDialog();
        console.log(this.LOG_TAG + "::init");
        
        // set up intent handlers
        intent.matches(/mm+/, [
            function (session) {
                session.send("...si lo sé! es raro hablar con un bot :)");
                session.endDialog();
            }
        ]);

        intent.matches(/jaja+/, [
            function (session) {
                session.send("Siempre es lindo sacarle una sonrisa al otro :)");
                session.endDialog();
            }
        ]);

        intent.matches(/gracias+/, [
            function (session) {
                session.send("De nada! Estoy para ayudarte ;)");
                session.endDialog();
            }
        ]);

        intent.matches(/(bien+)+(.*)+(vos)+(.*)+(\*?)/, [
            function (session) {
                session.send("bien, acá.. boteando.. gracias por preguntar!");
                session.beginDialog('rootMenu');
                session.endDialog();
            }
        ]);

        intent.matches(/(wtf)/, [
            function (session) {
                session.send("Ey.. no es para tanto!");
                session.endDialog();
            }
        ]);

        intent.matches(/profundo+/, [
            function (session) {
                session.send("Este tipo de cosas te hacen pensar.. no?");
                session.endDialog();
            }
        ]);


        intent.matches(/(gato)/, [
            function (session) {
                session.send("Ey.. gato es otra cosa!");
                session.endDialog();
            }
        ]);

        intent.onDefault([
            function (session, args, next) {
                console.log("Default Random Dialog");
                if (!session.conversationData.hello) {
                    session.beginDialog('firstRun');
                } else {
                    if (!session.conversationData.menuDisplayed) {
                        session.conversationData.menuDisplayed = true;
                        session.beginDialog('rootMenu');
                    } else {
                        console.log("RandomIntentHandler::send random text " + randomReplies.messages.length);
                        

                        var random_index = self.getRandomInt(0, randomReplies.messages.length);

                        if (!session.conversationData.lastRandomPhrase) {
                            // send random message to chat window
                            session.send(randomReplies.messages[random_index]);
                            // store last random phrase so it doesn't get repeated next time
                            session.conversationData.lastRandomPhrase = random_index;
                        } else {
                            while(random_index != session.conversationData.lastRandomPhrase) {
                                // send random message to chat window
                                session.send(randomReplies.messages[random_index]);
                                // store last random phrase so it doesn't get repeated next time
                                session.conversationData.lastRandomPhrase = random_index;
                            }
                        }
                        
                        // Control how many random replies we return to the user 
                        // so the experience doesn't become to random and crazy :)
                        if (!session.conversationData.randomRepliesCount) {
                            session.conversationData.randomRepliesCount = 1;
                        } else {
                            if (session.conversationData.randomRepliesCount >= 2) {
                                session.conversationData.randomRepliesCount = 0;
                                session.conversationData.menuDisplayed = false;
                            } else {
                                session.conversationData.randomRepliesCount += 1;
                            }
                        }

                        // end random dialog
                        session.endDialog();
                    }
                }
            }
        ]);

        GlobalBotInstance.dialog('/random', intent);
    },

    getRandomInt: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
};

module.exports = new RandomIntentHandler ();