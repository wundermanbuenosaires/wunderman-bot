// BotFramework dependencies
var builder = require('botbuilder');

var IntentController = function (bot) {
    this.LOG_TAG = "IntentController::";

    console.log(this.LOG_TAG + "new IntentController instance");
    this.init();
};


IntentController.prototype = {
    init: function () {
        console.log(this.LOG_TAG + "init");

        // configure all intent handlers by type
        // and add its config to the bot instance

        // intialize Luis Intent Handler
        var LuisIntentHandler = require('./LuisIntentHandler');

        // intialize Random Intent Handler
        var RandomIntent = require('./RandomIntentHandler');
    }
}

module.exports = IntentController;