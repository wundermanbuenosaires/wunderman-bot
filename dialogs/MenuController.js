// bot dependencies
var builder = require('botbuilder');

// MenuControler
var MenuController = function (bot) {
    this.LOG_TAG = "HelloController::";
    this.init();
};

MenuController.prototype = {
    // init
    init: function () {
        // Add root menu dialog
        GlobalBotInstance.dialog('rootMenu', [
            function (session) {
                builder.Prompts.choice(session, "Puedo ofrecerte información sobre alguno de estos temas:", [
                    "Saber más sobre Wunderman",
                    "Trabajar en Wunderman",
                    "Ofrecer Servicios a Wunderman",
                    "Involucrar a Wunderman en un Proyecto"
                ]);
            },
            function (session, results) {
                console.log("rootMenu::index: " + results.response.index);
                if (results && results.response && results.response.index >= 0) {
                    switch (results.response.index) {
                        case 0:
                            session.beginDialog("about");
                            break;
                        case 1:
                            session.beginDialog("/hr_profile");
                            break;
                        case 2:
                            session.send("Qué tipo de servicios estás ofreciendo?");
                            session.endDialog();
                            break;
                        case 3:
                            session.send("Podés enviarle la propuesta a nb@wunderman.com.ar");
                            session.endDialog();
                            break;
                        default:
                            session.send("Con qué otra cosa te puedo ayudar?");
                            session.endDialog();
                            break;
                    }
                }
            },
            function (session) {
                // end menu
                session.endDialog();
            }
        ]);
    }
};

module.exports = new MenuController();
