// bot dependencies
var builder = require('botbuilder');
var request = require('request');
var config = require('../config');
var Util = require("../utils/Functions");

// HelloController
var AboutController = function (bot) {
    this.LOG_TAG = "AboutController::";
    this.init();
};

AboutController.prototype = {
    // init
    init: function () {
        var self = this;
        // Add first run dialog
        GlobalBotInstance.dialog('about', [
            function (session) {
                builder.Prompts.choice(session, "Qué te interesa saber sobre Wunderman?", [
                    "Nuestros Trabajos y Clientes",
                    "Leadership",
                    "Servicios que brindamos",
                    "Otros temas"
                ]);
            },
            function (session, results) {
                console.log("rootMenu::index: " + results.response.index);
                switch (results.response.index) {
                    case 0:
                        self.getClients(function(error, data) {
                            session.beginDialog("clients", data);
                            session.endDialog();
                        });
                        break;
                    case 1:
                        self.getLeadership(function(error, data) {
                            session.beginDialog("leadership", data);
                            session.endDialog();
                        });
                        break;
                    case 2:
                        session.send("Wunderman es hoy la agencia líder en el mercado local de marketing relacional y digital, tanto por su enfoque estratégico como por sus capacidades productivas y de implementación.");
                        session.send("Combinamos data y creatividad para responder a las necesidades de una nueva generación de clientes, incorporando las últimas innovaciones y tendencias del mercado. Eso se refleja en la diversidad de nuestras áreas y servicios.");
                        session.send("Conocé más en: http://wunderman.com.ar/es/quienes-somos");
                        session.endDialog();
                        break;
                    case 3:
                        builder.Prompts.choice(session, "Veamos.. cuál es tu pregunta?");
                        break;
                    default:
                        builder.Prompts.choice(session, "Veamos.. cuál es tu pregunta?");
                        break;
                }
            },
            function (session, results) {
                if (session.response) {
                    session.send(results.response + " es una buena pregunta");
                    session.endDialog();
                }
            }
        ]);


        // Add first run dialog
        GlobalBotInstance.dialog('about-luis', [
            function (session) {
                if (session.conversationData.People) {
                    var personMatch = self.searchInLeadership(session.conversationData.People, function (error, personMatch) {
                        if (personMatch && personMatch.length > 0) {
                            session.conversationData.personMatch = personMatch;
                            session.beginDialog('person-card');
                            session.endDialog();
                        } else {
                            session.send("Lamentablemente no encontramos información sobre el " + session.conversationData.People);
                            session.send("Podés probar suerte ingresando a http://wunderman.com.ar/quienes-somos");
                        }
                    });
                }
            }
        ]);

        // Add first run dialog
        GlobalBotInstance.dialog('person-card', [
            function (session) {
                var data = session.conversationData.personMatch,
                    cards = [];

                console.log(self.LOG_TAG + "::person-card");
                console.log(data);

                for(var i = 0; i < data.length; i ++) {

                    var card = new builder.HeroCard(session)
                        .title(data[i].name)
                        .subtitle(data[i].title)
                        .text("Para conocer más sobre nuestro " + Util.capitalizeFirstLetter(session.conversationData.People) + " podés visitar http://wunderman.com.ar/quienes-somos")
                        .images([
                            builder.CardImage.create(session, data[i].picture)
                        ]);

                    // add card to array
                    cards.push(card);
                }

                var msg = new builder.Message(session);
                msg.attachmentLayout(builder.AttachmentLayout.carousel);
                msg.attachments(cards);

                session.send(msg);

                // cleanup search
                session.endDialog();
            }
        ]);

        // Add first run dialog
        GlobalBotInstance.dialog('leadership', [
            function (session, data) {
                var cards = [];

                console.log(self.LOG_TAG + "::leadership");
                console.log(data);

                for(var i = 0; i < data.length; i ++) {

                    var card = new builder.HeroCard(session)
                        .title(data[i].name)
                        .subtitle(data[i].title)
                        .text("Para conocer más sobre nuestro Leadership podés visitar http://wunderman.com.ar/quienes-somos")
                        .images([
                            builder.CardImage.create(session, data[i].picture)
                        ]);

                    // add card to array
                    cards.push(card);
                }

                var msg = new builder.Message(session);
                msg.attachmentLayout(builder.AttachmentLayout.carousel);
                msg.attachments(cards);

                session.send(msg);

                // cleanup search
                session.conversationData.People = null;
                session.endDialog();
            }
        ]);

        // Add first run dialog
        GlobalBotInstance.dialog('clients', [
            function (session, data) {
                var cards = [];

                console.log(self.LOG_TAG + "::clients");
                console.log(data);

                session.send("Estos son algunos de nuestros clientes:");

                for(var i = 0; i < data.length; i ++) {

                    var card = new builder.HeroCard(session)
                        .images([
                            builder.CardImage.create(session, data[i].picture)
                        ]);

                    // add card to array
                    cards.push(card);
                }

                var msg = new builder.Message(session);
                msg.attachmentLayout(builder.AttachmentLayout.carousel);
                msg.attachments(cards);

                session.send(msg);
                session.send("Te invito a que conozcas la sección 'Nuestro Trabajo' dentro de nuestro sitio web. Ingresá a http://wunderman.com.ar/es/nuestro-trabajo para conocer toda la información sobre nuestros servicios y los clientes para los que trabajamos.");
                
                session.endDialog();
            }
        ]);
    },

    searchInLeadership: function (candidate, cb) {
        var self = this;
        var match = [];
        
        console.log(this.LOG_TAG + "::peopleLookup");
        
        request(config.API_URL + "/about/people", function (error, response) {
            candidate = self.transformCandidate(candidate.toLowerCase());

            if (!error && response.statusCode == 200) {
                var list = JSON.parse(response.body);
                console.log(self.LOG_TAG + "::peopleLookup: results back: " + list.length);
                for (var i = 0; i < list.length; i ++) {
                    var title = list[i].title;
                    console.log(title.toLowerCase() + " indexOf("+ candidate +")");
                    if (title.indexOf(candidate) >= 0) {
                        match.push(list[i]);
                    }
                }
                cb(error, match);
            }
        });
    },

    getLeadership: function(cb) {
        request(config.API_URL + "/about/people", function (error, response) {
            if (!error && response.statusCode == 200) {
                var list = JSON.parse(response.body);
                cb(error, list);
            }
        });
    },

    getClients: function(cb) {
        request(config.API_URL + "/about/clients", function (error, response) {
            if (!error && response.statusCode == 200) {
                var list = JSON.parse(response.body);
                cb(error, list);
            }
        });
    },

    transformCandidate: function (str) {
        if (str === "presidente") {
            str = "President";
        }

        if (str === "vicepresidente") {
            str = "Vicepresident";
        }

        if (str === "vp") {
            str = "Vicepresident";
        }

        if (str === "dgc") {
            str = "Executive Creative Director";
        }

        return str;
    }
};

module.exports = new AboutController();
