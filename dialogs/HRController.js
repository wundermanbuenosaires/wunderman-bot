// bot dependencies
var builder = require('botbuilder');
// other dependencies
var request = require('request');
var config = require('../config');
var Util = require("../utils/Functions");

// HRControler JS
var HRController = function (bot) {
    this.LOG_TAG = "HRController::";
    this.init();
};

HRController.prototype = {
    // init
    init: function () {
        var self = this;

        /* HR Dialog Main Thread */
        GlobalBotInstance.dialog('/hr_profile', [
            // starts by asking the user for its name.
            function (session, args, next) {
                if (!session.userData.name) {
                    builder.Prompts.text(session, 'Bien. Empecemos por lo básico, cuál es tu nombre?');
                } else {
                    console.log("next1");
                    next();
                }
            },

            // process name and stores it in the bot session.
            function (session, results, next) {
                if (!session.userData.name && results.response != null && typeof results.response === "string" 
                    && results.response != undefined) {
                    console.log("set name to " + results.response);
                    session.userData.name = results.response;
                }

                if (session.userData.name) {
                    console.log("next2");
                    next();
                }
            },

            // ask user about its professional profile
            function (session, args, next) {
                if (!session.conversationData.profile || session.conversationData.profile.length === 0) {
                    console.log("bot:dialog::profile posición");
                    builder.Prompts.text(session, "Qué tipo de posición estás buscando " + session.userData.name + "? Describí el perfil o la posición en una palabra, por ej 'Desarrollador'");
                } else {
                    next();
                }
            },

            // receives text response from the user
            // or predictions from LUIS API.
            function (session, results, next) {
                if (results.response && typeof results.response === "string") {
                    session.conversationData.profile = self.searchJobInResponse(results.response);   
                }

                session.beginDialog("/jobList");
                session.endDialog();
            }
        ]);

        GlobalBotInstance.dialog('/email_request', [
            function (session) {
                builder.Prompts.text(session, 'Si me puedes compartir tu email, puedo enviarle un email a la persona que se encarga de esto.');
            },
            function (session, results) {
                if (result.response.indexOf("@") > 0) {
                    session.userData.email = results.response;
                } else {
                    session.send('Si no te sentís cómodo dandole tus datos a un Bot, está bien ;)');
                    session.send('Podés usar nuestro formulario de contacto ubicado en nuestra web: http://wunderman.com.ar/');
                }
            }
        ]);

        /* Based on LUIS or HR Questionnarie 
        ** prompts the user with a list of available Job openings
        ** based on its profile (Developer, Designer, etc)
        */
        GlobalBotInstance.dialog('/jobList', [
            // now we decided based on the users profile
            function (session, results, next) {
                if (!session.conversationData.menuDisplayed && session.userData.name) {
                    session.send("Muy bien " + Util.capitalizeFirstLetter(session.userData.name) + ", dejame buscar dentro del listado de posiciones abiertas para tu perfil, " + session.conversationData.profile);
                } else {
                 session.send("Dejame buscar dentro del listado de posiciones abiertas para tu perfil, " + session.conversationData.profile);
                }
                
                session.send("un segundo por favor..");
                
                self.jobLookup(session.conversationData.profile.toLowerCase(), function (error, list) {
                    if (!error) {
                        session.conversationData.jobList = list;
                        if (list.length > 0) {
                            // next to job opening list propmt
                            console.log("jobList:1::next");
                            session.beginDialog("/jobPickList");
                            //session.endDialog();
                        } else {
                            // clear data
                            session.conversationData.profile = null;
                            session.send("No encontramos búsquedas activas para tu perfil. No te desanimes! Podés enviarle tu cv a rrhhargentina@wunderman.com");
                            session.endDialog();
                        }
                    } else {
                        // clear data
                        session.conversationData.profile = null;
                        session.send("Oops ocurrió un error :() No te desanimes! Podés enviarle tu cv a rrhhargentina@wunderman.com y estaremos nos pondremos en contacto apenas podamos!");
                        session.endDialog();   
                    }
                });
            }
        ]);
        
        // job list prompt builder
        GlobalBotInstance.dialog('/jobPickList', [
            function (session) {
                var list = session.conversationData.jobList,
                    options = self.generateJobOptions(list);
                // prompt options to the user
                builder.Prompts.choice(session, "Encontramos " + list.length + " posiciones que pueden interesarte:", options);
            },

            // process user response
            function (session, results) {
                console.log("HRResults::jobPickList:response");
                console.log(results);
                if (results && results.response && results.response.index >= 0) {
                    var responseIndex = results.response.index;
                    // if responseIndex < list.length 
                    // it means the user picked an item from the actual openings
                    // otherwise it selected the fallback response, not interested in any of the options and triggers the ELSE statement.
                    console.log(responseIndex + " <  " + session.conversationData.jobList.length);
                    if (responseIndex < session.conversationData.jobList.length) {
                        session.send("Te interesa " + session.conversationData.jobList[results.response.index].title);
                    
                    } else {
                        session.send("No te desanimes! Podés enviarle tu cv a rrhhargentina@wunderman.com y te notificaremos cuándo tengamos una búsqueda que pueda interesarte");
                    }

                    // end HR FLow and Reset menuDisplayed flag and Profile data
                    session.conversationData.profile = null;
                    session.conversationData.menuDisplayed = false;
                    session.endDialog();
                }
            },
        ]);
    },

    // Searchjob profile matches within a provided String
    searchJobInResponse: function (str) {
        console.log(this.LOG_TAG + " searchJobInResponse:: " + str);
        // to do: search job profile within user response String.
        return str;
    },

    // Use jobLookup results to build a multiple-choice prompt to the user
    generateJobOptions: function (list) {
        var options = [];

        for (var i = 0; i < list.length; i ++) {
            options.push(list[i].title);
        }

        // add fallback option
        options.push("No me interesa ninguna de las posiciones.");

        return options;
    },

    /* Scrape wunderman.com.ar and return an array of job openings */
    /* api routes configured within /api/index.js router filer */
    jobLookup: function (jobDesc, cb) {
        var self = this;
        var matches = [];
        
        console.log(this.LOG_TAG + "::jobLookup");
        
        request(config.API_URL + "/hr/openings", function (error, response) {
            if (!error && response.statusCode == 200) {
                var list = JSON.parse(response.body);
                console.log(self.LOG_TAG + "::jobLookup: results back: " + list.length);
                for (var i = 0; i < list.length; i ++) {
                    var title = list[i].title;
                    if (title.toLowerCase().indexOf(jobDesc) >= 0) {
                        matches.push(list[i]);
                    }
                }
                cb(error, matches);
            }
        });
    }
};

module.exports = new HRController ();
