// bot dependencies
var builder = require('botbuilder');

// HelloController
var HelloController = function (bot) {
    this.LOG_TAG = "HelloController::";
    this.init();
};

HelloController.prototype = {
    // init
    init: function () {
        // Add first run dialog
        GlobalBotInstance.dialog('firstRun', [
            function (session) {
                console.log("bot:dialog::firstRun");
                // Update versio number and start Prompts
                // - The version number needs to be updated first to prevent re-triggering 
                //   the dialog. 
                session.userData.version = 1.0; 


                session.beginDialog("/welcome");
            }
        ]);

        // welcome message dialog
        GlobalBotInstance.dialog("/welcome", [
            function (session) {
                console.log("bot:dialog::welcome");
                if (session.userData.name) {
                    session.send("Bienvenido %s ! En qué te puedo ayudar?", session.userData.name);
                    session.conversationData.hello = true;
                } else {
                    session.send("Hola cómo estás? Soy WunBot, el bot de Wunderman.");
                    session.send("¿En qué te puedo ayudar?");
                    session.conversationData.hello = true;
                }

                session.endDialog();
            }
        ]);
    }
};

module.exports = new HelloController();
