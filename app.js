var express = require('express')

// BotFramework dependencies
var builder = require('botbuilder');

var IntentHandler = require("./intents");
var DialogHandler = require("./dialogs");

// Setup Express Server
var app = express();

//=========================================================
// Aux API Setup
//=========================================================
var api = require('./api');
app.use('/api', api);

//=========================================================
// Bot Setup
//=========================================================

// Create chat bot
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

/** IMPORTANT - bot variable is GLOBAL !! **/
GlobalBotInstance = new builder.UniversalBot(connector);
GlobalBotInstance.set("persistConversationData", true);
GlobalBotInstance.set("persistUserData", true);

// Express Handler Set Up
app.post('/api/messages', connector.listen());

// load interactions
var intentHandler = new IntentHandler();
var dialogHandler = new DialogHandler();

//=========================================================
// HTTP Test
//=========================================================
app.get('/', function (req, res, next) {
	res.send('Express app.');
});

app.listen(process.env.PORT || 3000, function () {
  console.log('App listening on port ' + process.env.PORT || 3000);
});
