var express = require('express');
var router = express.Router();
var WebScraper = require('../utils/WebScraper');

/* GET HR openings from wunderman.com.ar page. */
router.get('/hr/openings', function(req, res, next) {
	console.log("api::request /hr/openings");
	WebScraper.extractHROpenings(function (error, openings) {
		if (!error) {
			res.send(openings);
		} else {
			res.send("Something went wrong!");
		}
	});
});

/* GET Leadership Matching info from wunderman.com.ar page. */
router.get('/about/people', function(req, res, next) {
	console.log("api::request /about/people");
	WebScraper.extractPeopleInfo(function (error, data) {
		if (!error) {
			res.send(data);
		} else {
			res.send("Something went wrong!");
		}
	});
});

/* GET Client  info from wunderman.com.ar page. */
router.get('/about/clients', function(req, res, next) {
	console.log("api::request /about/clients");
	WebScraper.extractClientsInfo(function (error, data) {
		if (!error) {
			res.send(data);
		} else {
			res.send("Something went wrong!");
		}
	});
});

module.exports = router;