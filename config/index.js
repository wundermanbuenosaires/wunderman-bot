module.exports = {
	API_URL: "https://wunderman-bot.azurewebsites.net/api",
	HR_URL: "http://wunderman.com.ar/es/ofertas-laborales",
	LEADERSHIP_URL: "http://wunderman.com.ar/es/quienes-somos",
	ABOUT_URL: "http://wunderman.com.ar/es/nuestro-trabajo",
	LUIS_URL: 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/72ed05ba-b879-4d2b-af21-a587d367c4e5?subscription-key=58de3e3142e34b62842617b078aa5c23&verbose=true&timezoneOffset=0&q='
};